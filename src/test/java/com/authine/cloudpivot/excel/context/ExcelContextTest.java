package com.authine.cloudpivot.excel.context;

import com.authine.cloudpivot.excel.ExcelBuilder;
import org.apache.poi.ooxml.util.PackageHelper;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.*;
import java.util.*;

/**
 * ExcelContext测试类
 *
 * @author xukai
 * @date 2021/12/08 18:30
 **/
public class ExcelContextTest {

    @Test
    public void excelContextTest() throws IOException {
        List<Map<String, Object>> xk = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Map<String, Object> xkMap = new HashMap<>();
            xkMap.put("name", "xukai"+i);
            xkMap.put("age", i);
            xk.add(xkMap);
        }
        List<Map<String, Object>> xc = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Map<String, Object> xcMap = new HashMap<>();
            xcMap.put("name", "xuchen"+i);
            xcMap.put("age", i);
            xc.add(xcMap);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("name", "xukai");
        map.put("age", 2);
        map.put("xk", xk);
        map.put("xc", xc);
        map.put("dataTime", new Date());
        Resource resource = new ClassPathResource("testTemplate.xlsx");
        InputStream inputStream = resource.getInputStream();
        ExcelContext excelContext = ExcelBuilder.buildExcelContext(inputStream);
        excelContext.intTemplateWorkBook();
        excelContext.write(map);

        excelContext.finish(new FileOutputStream("C:\\Users\\xukai\\Desktop\\" + System.currentTimeMillis() + "testTemplete.xlsx"));
    }

    @Test
    public void testTemplateExcelParse() {

    }
}

