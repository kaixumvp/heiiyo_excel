package com.authine.cloudpivot.excel.holder;

import com.authine.cloudpivot.excel.cell.ExcelCell;
import lombok.Data;

/**
 * 单元格持有者
 *
 * @author xukai
 * @date 2021/12/08 14:40
 **/
@Data
public class CellHolder {
    private ExcelCell excelCell;

}

