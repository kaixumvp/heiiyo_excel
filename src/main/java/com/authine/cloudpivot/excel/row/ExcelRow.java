package com.authine.cloudpivot.excel.row;

import com.authine.cloudpivot.excel.cell.ExcelCell;
import lombok.Data;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * 行
 *
 * @author xukai
 * @date 2021/12/08 14:42
 **/
@Data
public class ExcelRow extends AbstractExcelRow{

    @Override
    public void handleData(Object data) {
        Map dataMap = (Map) data;
        Row row = this.createRow(this);
        this.setIndexNum(this.getExcelSheet().getLastSheetIndex());
        this.getExcelSheet().setLastSheetIndex(this.getExcelSheet().getLastSheetIndex() + 1);
        List<ExcelCell> excelCells = this.getCells();
        ListIterator<ExcelCell> excelCellListIterator = excelCells.listIterator();
        while (excelCellListIterator.hasNext()){
            ExcelCell excelCell = excelCellListIterator.next();
            excelCell.setExcelRow(this);
            excelCell.setValue(dataMap.get(excelCell.getTagField()));
            Cell cell = row.getCell(excelCell.getColumnNum());
            if (cell == null){
                cell = row.createCell(excelCell.getColumnNum());
            }
            excelCell.handleData(cell);
        }
        this.getExcelSheet().handleMergedRegions(this);
    }


}

