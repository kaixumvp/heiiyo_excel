package com.authine.cloudpivot.excel.row;

import com.authine.cloudpivot.excel.cell.ExcelCell;
import com.authine.cloudpivot.excel.listener.able.ExcelListenerAble;
import com.authine.cloudpivot.excel.sheet.ExcelSheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.List;

/**
 * 抽象类
 *
 * @author xukai
 * @date 2021/12/13 14:45
 **/
public abstract class AbstractExcelRow implements ExcelRowInterface{

    //excel开始行的位置
    private int startIndex;

    //偏移量-争对自己的数据便宜
    private int offset;

    //模板行的单元格数据
    private List<ExcelCell> excelTemCells;

    //模板行
    private Row row;

    //所属模板sheet对象
    private ExcelSheet excelSheet;

    ExcelListenerAble excelListenerAble;

    @Override
    public List<ExcelCell> getCells() {
        return excelTemCells;
    }

    @Override
    public void setCells(List<ExcelCell> excelCells) {
        this.excelTemCells = excelCells;
    }

    @Override
    public void setIndexNum(int num) {
        this.startIndex = num;
    }

    @Override
    public int getIndexNum() {
        return this.startIndex;
    }

    @Override
    public int getOffset() {
        return offset;
    }

    @Override
    public void setOffSet(int offSet) {
        this.offset = offSet;
    }


    @Override
    public Row getRow() {
        return this.row;
    }

    @Override
    public void setRow(Row row) {
        this.row = row;
    }

    @Override
    public void getSheet() {
        row.getSheet();
    }

    public ExcelSheet getExcelSheet() {
        return excelSheet;
    }

    public void setExcelSheet(ExcelSheet excelSheet) {
        this.excelSheet = excelSheet;
    }

    public Row createRow(ExcelRowInterface excelRow){
        Sheet sheet = this.row.getSheet();
        Row rowNew = sheet.getRow(this.excelSheet.getLastSheetIndex());
        if (rowNew == null){
            rowNew = sheet.createRow(this.excelSheet.getLastSheetIndex());
        }
        Row rowTem = excelRow.getRow();
        rowNew.setHeight(rowTem.getHeight());
        rowNew.setRowStyle(rowTem.getRowStyle());
        rowNew.setZeroHeight(rowTem.getZeroHeight());
        rowNew.setHeightInPoints(rowTem.getHeightInPoints());
        return rowNew;
    }
}

