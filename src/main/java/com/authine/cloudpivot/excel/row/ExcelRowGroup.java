package com.authine.cloudpivot.excel.row;

import com.alibaba.fastjson.JSON;
import com.authine.cloudpivot.excel.cell.ExcelCell;
import lombok.Data;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * 行分组
 *
 * @author xukai
 * @date 2021/12/08 17:51
 **/
@Data
public class ExcelRowGroup extends AbstractExcelRow{
    //子表名称
    private String subName;

    //行组拥有的行数据
    private List<ExcelRow> excelRows;

    @Override
    public void handleData(Object data) {
        Map dataMap = (Map) data;
        Object rowData = dataMap.get(this.subName);
        if (!(rowData instanceof List)){
        }
        List rowDataList  = (List) rowData;
        if (rowData == null){
            return;
        }
        //单元格
        List<ExcelCell> excelRows = this.getCells();
        int rowOffset = 0;
        for (Object rowItem : rowDataList) {
            if (rowOffset == 0){
                this.setIndexNum(this.getExcelSheet().getLastSheetIndex());
            }
            this.setOffSet(rowOffset);
            Row row = this.createRow(this);
            Map item = JSON.parseObject(JSON.toJSONString(rowItem), Map.class);
            ListIterator<ExcelCell> excelCellListIterator = excelRows.listIterator();
            while (excelCellListIterator.hasNext()){
                ExcelCell excelCell = excelCellListIterator.next();
                excelCell.setValue(item.get(excelCell.getTagField()));
                excelCell.setOffset(rowOffset);
                Cell cell = row.getCell(excelCell.getColumnNum());
                if (cell == null){
                    cell = row.createCell(excelCell.getColumnNum());
                }
                excelCell.handleData(cell);
            }
            rowOffset ++;
            this.getExcelSheet().setLastSheetIndex(this.getIndexNum() + rowOffset);
        }
        this.getExcelSheet().handleMergedRegions(this);
    }

}

