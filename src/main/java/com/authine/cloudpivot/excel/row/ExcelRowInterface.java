package com.authine.cloudpivot.excel.row;

import com.authine.cloudpivot.excel.cell.ExcelCell;
import com.authine.cloudpivot.excel.sheet.ExcelSheet;
import org.apache.poi.ss.usermodel.Row;

import java.util.List;
import java.util.Map;

public interface ExcelRowInterface {

    /**
     * 获取所有的单元格
     * @author xukai
     * @date 2021/12/16 14:48
     * @return java.util.List<com.authine.cloudpivot.excel.cell.ExcelCell>
     */
    List<ExcelCell> getCells();

    /**
     * 设置单元格
     * @author xukai
     * @date 2021/12/16 14:48
     * @param excelCells
     * @return void
     */
    void setCells(List<ExcelCell> excelCells);

    /**
     * 设置sheet中所在的位置
     * @author xukai
     * @date 2021/12/16 14:48
     * @param num
     * @return void
     */
    void setIndexNum(int num);

    /**
     * 获取所在的位置
     * @author xukai
     * @date 2021/12/16 14:49
     * @return int
     */
    int getIndexNum();

    /**
     * 或者自身的数据的偏移量
     * @author xukai
     * @date 2021/12/16 14:50
     * @return int
     */
    int getOffset();

    /**
     * 设置自身数据的偏移量
     * @author xukai
     * @date 2021/12/16 14:50
     * @param offSet
     * @return void
     */
    void setOffSet(int offSet);

    void handleData(Object data);

    /**
     * 获取到poi的row
     * @author xukai
     * @date 2021/12/16 14:51
     * @return org.apache.poi.ss.usermodel.Row
     */
    Row getRow();

    /**
     * 设置绑定的poirow
     * @author xukai
     * @date 2021/12/16 14:51
     * @param row
     * @return void
     */
    void setRow(Row row);

    void getSheet();

    public ExcelSheet getExcelSheet();

    public void setExcelSheet(ExcelSheet excelSheet);
}
