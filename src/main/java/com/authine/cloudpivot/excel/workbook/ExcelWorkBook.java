package com.authine.cloudpivot.excel.workbook;

import com.authine.cloudpivot.excel.sheet.ExcelSheet;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 工作簿
 *
 * @author xukai
 * @date 2021/12/09 10:29
 **/
@Data
public class ExcelWorkBook {

    public ExcelWorkBook(){
        this.excelSheets = new ArrayList<>();
    }

    private String bookName;

    private List<ExcelSheet> excelSheets;

    public ExcelSheet getSheetByIndex(int index){
        return excelSheets.get(index);
    }
}

