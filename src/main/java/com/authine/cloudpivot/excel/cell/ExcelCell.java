package com.authine.cloudpivot.excel.cell;

import com.authine.cloudpivot.excel.listener.able.ExcelListenerAble;
import com.authine.cloudpivot.excel.listener.event.ExcelEvent;
import com.authine.cloudpivot.excel.listener.event.ExcelEventObject;
import com.authine.cloudpivot.excel.row.ExcelRowInterface;
import lombok.Data;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;

import java.util.Date;

/**
 * excel 单元格描述
 *
 * @author xukai
 * @date 2021/12/08 14:33
 **/
@Data
public class ExcelCell {

    //是否title类型
    private boolean isTitle;

    //是否序列号类型
    private boolean isIdx;

    //行位置
    private int rowNum;

    //偏移量-争对自己的数据便宜
    private int offset;

    //列位置
    private int columnNum;

    //标签模板{}
    private String tagTem;

    //字段名称
    private String tagField;

    //值
    private Object value;

    //表格样式
    private CellStyle cellStyle;

    //表格类型
    private CellType cellType;

    //所属行
    ExcelRowInterface excelRow;

    ExcelListenerAble excelListenerAble;

    public void handleData(Cell cell){
        if (cell != null) {
            if (excelListenerAble != null){
                excelListenerAble.startListener(new ExcelEventObject(ExcelEvent.CELL_VALUE_BEFORE, this));
            }
            cell.setCellType(cellType);
            cell.setCellStyle(cellStyle);
            if (isTitle){
                cell.setCellValue(tagTem);
                return;
            }
            if (isIdx){
                cell.setCellValue(offset + Integer.parseInt(tagField));
                return;
            }
            if (value != null){
                if (value instanceof Date){
                    cell.setCellValue((Date) value);
                }else {
                    cell.setCellValue(value.toString());
                }
            }else {
                cell.setCellValue("");
            }

        }
    }


}

