package com.authine.cloudpivot.excel.sheet;

import com.alibaba.fastjson.JSON;
import com.authine.cloudpivot.excel.cell.ExcelCell;
import com.authine.cloudpivot.excel.listener.able.ExcelListenerAble;
import com.authine.cloudpivot.excel.listener.event.ExcelEvent;
import com.authine.cloudpivot.excel.listener.event.ExcelEventObject;
import com.authine.cloudpivot.excel.row.ExcelRow;
import com.authine.cloudpivot.excel.row.ExcelRowGroup;
import com.authine.cloudpivot.excel.row.ExcelRowInterface;
import lombok.Data;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.util.*;
import java.util.stream.Collectors;

/**
 * sheet
 *
 * @author xukai
 * @date 2021/12/09 10:31
 **/
@Data
public class ExcelSheet {

    public ExcelSheet(){
        this.excelRows = new ArrayList<>();
    }

    private String sheetName;

    private int index;

    private Sheet sheet;

    private int lastSheetIndex;

    private ExcelListenerAble excelListenerAble;

    List<ExcelRowInterface> excelRows;

    private CellStyle rowStyle;

    List<CellRangeAddress> mergedRegions;

    public void handleData(Map data){
        excelListenerAble.startListener(new ExcelEventObject(ExcelEvent.SHEET_VALUE_BEFORE, this));
        for (ExcelRowInterface excelRow : excelRows){
            excelRow.handleData(data);
        }
        this.lastSheetIndex--;
        int lastRowNum = sheet.getLastRowNum();
        //模板行大于最大数据行
        if (lastRowNum > lastSheetIndex){
            for (; lastRowNum > lastSheetIndex; lastRowNum--){
                sheet.removeRow(sheet.getRow(lastRowNum));
            }
        }
        excelListenerAble.startListener(new ExcelEventObject(ExcelEvent.SHEET_VALUE_AFTER, this));
    }

    public void handleMergedRegions(ExcelRowInterface excelRow){
        List<CellRangeAddress> mergedRegions = this.getMergedRegions();
        if (CollectionUtils.isEmpty(mergedRegions)){
            return;
        }
        //模板行
        int rowNum = excelRow.getRow().getRowNum();
        //实际行
        int indexNum = excelRow.getIndexNum();
        Map<Integer, List<CellRangeAddress>> cellRangeMap = mergedRegions.stream().collect(Collectors.groupingBy(CellRangeAddress::getFirstRow));
        List<CellRangeAddress> cellRangeAddresses = cellRangeMap.get(rowNum);
        if (CollectionUtils.isNotEmpty(cellRangeAddresses)){
            for (CellRangeAddress cellAddresses : cellRangeAddresses){
                sheet.addMergedRegion(new CellRangeAddress(indexNum, cellAddresses.getLastRow() + indexNum - rowNum,
                        cellAddresses.getFirstColumn(), cellAddresses.getLastColumn()));
            }
        }
    }


}

