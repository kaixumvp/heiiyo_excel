package com.authine.cloudpivot.excel.listener.event;

public enum ExcelEvent {

    CELL_VALUE_BEFORE, SHEET_VALUE_BEFORE,  SHEET_VALUE_AFTER;
}
