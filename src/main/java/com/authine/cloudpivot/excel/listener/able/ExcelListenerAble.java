package com.authine.cloudpivot.excel.listener.able;

import com.authine.cloudpivot.excel.listener.ExcelListener;
import com.authine.cloudpivot.excel.listener.event.ExcelEventObject;

public interface ExcelListenerAble {

    void addListener(ExcelListener listener);

    <E extends ExcelEventObject> void startListener(E excelEventObject);
}
