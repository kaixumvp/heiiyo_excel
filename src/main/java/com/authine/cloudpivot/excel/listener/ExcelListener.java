package com.authine.cloudpivot.excel.listener;

import com.authine.cloudpivot.excel.listener.event.ExcelEventObject;

public interface ExcelListener {

    void handle(ExcelEventObject excelEventObject);
}
