package com.authine.cloudpivot.excel.listener.event;

import com.authine.cloudpivot.excel.cell.ExcelCell;

/**
 * 时间对象
 *
 * @author xukai
 * @date 2021/12/13 15:29
 **/
public class ExcelEventObject {


    public ExcelEventObject(ExcelEvent excelEvent, Object excelObject){
        this.excelEvent = excelEvent;
        this.excelObject = excelObject;
    }

    private ExcelEvent excelEvent;

    private Object excelObject;

    public ExcelEvent getExcelEvent() {
        return excelEvent;
    }

    public void setExcelEvent(ExcelEvent excelEvent) {
        this.excelEvent = excelEvent;
    }

    public Object getExcelObject() {
        return excelObject;
    }

    public void setExcelObject(Object excelObject) {
        this.excelObject = excelObject;
    }
}

