package com.authine.cloudpivot.excel.listener.able;

import com.authine.cloudpivot.excel.listener.ExcelListener;
import com.authine.cloudpivot.excel.listener.event.ExcelEventObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 默认期初监听器启动者
 *
 * @author xukai
 * @date 2021/12/13 15:18
 **/
public abstract class BaseExcelListenerAble implements ExcelListenerAble {

     private List<ExcelListener> excelListeners = new ArrayList<>();

    @Override
    public void addListener(ExcelListener listener) {
        excelListeners.add(listener);
    }

    @Override
    public void startListener(ExcelEventObject excelEvent) {
        for (ExcelListener excelListener : excelListeners){
            excelListener.handle(excelEvent);
        }
    }
}

