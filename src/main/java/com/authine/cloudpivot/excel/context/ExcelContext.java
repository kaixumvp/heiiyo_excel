package com.authine.cloudpivot.excel.context;

import com.authine.cloudpivot.excel.listener.able.ExcelListenerAble;
import com.authine.cloudpivot.excel.parse.TemplateExcelParse;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;
import java.io.OutputStream;

public interface ExcelContext extends ExcelListenerAble {

    /**
     * 数据写入
     * @author xukai
     * @date 2021/12/08 14:46
     * @return void
     */
    void write(Object... object);

    /**
     * 输出流
     * @author xukai
     * @date 2021/12/09 15:59
     * @param out
     * @return void
     */
    void finish(OutputStream out);

    /**
     * 获取模板工作簿
     * @author xukai
     * @date 2021/12/08 17:39
     * @return org.apache.poi.ss.usermodel.Workbook
     */
    Workbook getTemplateWorkBook();


    /**
     * 设置解析器
     * @author xukai
     * @date 2021/12/13 14:53
     * @param templateExcelParse
     * @return void
     */
    void setExcelParse(TemplateExcelParse templateExcelParse);

    /**
     * 初始化模板
     * @author xukai
     * @date 2021/12/09 15:59
     * @return void
     */
    void intTemplateWorkBook();

    /**
     * 设置文件格式
     * @author xukai
     * @date 2021/12/09 16:40
     * @param format
     * @return void
     */
    void setFormat(String format);

}
