package com.authine.cloudpivot.excel.context;

import com.alibaba.fastjson.JSON;
import com.authine.cloudpivot.excel.listener.able.BaseExcelListenerAble;
import com.authine.cloudpivot.excel.parse.BaseTemplateParse;
import com.authine.cloudpivot.excel.parse.TemplateExcelParse;
import com.authine.cloudpivot.excel.sheet.ExcelSheet;
import com.authine.cloudpivot.excel.workbook.ExcelWorkBook;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 基础ExcelContext
 *
 * @author xukai
 * @date 2021/12/08 14:44
 **/
public class BaseExcelContext extends BaseExcelListenerAble implements ExcelContext {

    private Workbook templateWorkbook;

    //解析器
    private TemplateExcelParse excelParse;

    /**
     * Template input stream
     * <p>
     * If 'inputStream' and 'file' all not empty,file first
     */
    private InputStream templateInputStream;
    /**
     * Template file
     * <p>
     * If 'inputStream' and 'file' all not empty,file first
     */
    private File templateFile;

    /**
     * 格式
     */
    private String format;

    private boolean isFinish;

    public static ExcelContext build(InputStream templateInputStream){
        return new BaseExcelContext(templateInputStream);
    }


    public static ExcelContext build(File templateFile){
        try {
            return new BaseExcelContext(new FileInputStream(templateFile), templateFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private BaseExcelContext(InputStream templateInputStream){
        this(templateInputStream, null);
    }

    private BaseExcelContext(InputStream templateInputStream, File file){
        this.isFinish = false;
        this.templateInputStream = templateInputStream;
        this.templateFile = file;
        this.format = "xlsx";
    }

    public void setFormat(String format){
        this.format = format;
    }

    public void setExcelParse(TemplateExcelParse excelParse){
        this.excelParse = excelParse;
    }

    @Override
    public void write(Object... objects) {
        if (excelParse == null){
            excelParse = new BaseTemplateParse();
        }
        ExcelWorkBook excelWorkBook = excelParse.toParse(this);
        if (objects ==null ||objects.length <= 0)
            return;
        for (int i = 0; i < objects.length; i++) {
            Map<String, Object> tableValue;
            if (objects[i] != null){
                if (objects[i] instanceof Map){
                    tableValue = (Map)objects[i];
                } else {
                    tableValue = JSON.parseObject(JSON.toJSONString(objects[i]), Map.class);
                }
                ExcelSheet excelSheet = excelWorkBook.getSheetByIndex(i);
                this.handleData(excelSheet, tableValue);
            }else {
                ExcelSheet excelSheet = excelWorkBook.getSheetByIndex(i);
                this.handleData(excelSheet, new HashMap());
            }

        }
        isFinish = true;
    }

    @Override
    public void finish(OutputStream out){
        try {
            if (this.templateWorkbook != null && isFinish){
                this.templateWorkbook.write(out);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                templateInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void intTemplateWorkBook() {
        if (Objects.isNull(templateWorkbook)){
            try {
                if (format.equals("xlsx")){
                    this.templateWorkbook = new XSSFWorkbook(this.templateInputStream);
                }else {
                    this.templateWorkbook = new HSSFWorkbook(this.templateInputStream);
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }

    }

    @Override
    public Workbook getTemplateWorkBook() {
        return this.templateWorkbook;
    }

    /**
     *
     * @author xukai
     * @date 2021/12/09 11:31
     * @param excelSheet 
     * @param data 
     * @return void 
     */
    private void handleData(ExcelSheet excelSheet, Map data){
        excelSheet.handleData(data);
    }

}

