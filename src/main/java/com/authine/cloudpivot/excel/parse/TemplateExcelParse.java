package com.authine.cloudpivot.excel.parse;

import com.authine.cloudpivot.excel.context.ExcelContext;
import com.authine.cloudpivot.excel.workbook.ExcelWorkBook;

/**
 * 模板解析器
 *
 * @author xukai
 * @date 2021/12/08 15:43
 **/
public interface TemplateExcelParse {

    ExcelWorkBook toParse(ExcelContext excelContext);
}

