package com.authine.cloudpivot.excel.parse;

import com.authine.cloudpivot.excel.cell.ExcelCell;
import com.authine.cloudpivot.excel.context.ExcelContext;
import com.authine.cloudpivot.excel.listener.able.ExcelListenerAble;
import com.authine.cloudpivot.excel.row.ExcelRow;
import com.authine.cloudpivot.excel.row.ExcelRowGroup;
import com.authine.cloudpivot.excel.row.ExcelRowInterface;
import com.authine.cloudpivot.excel.sheet.ExcelSheet;
import com.authine.cloudpivot.excel.workbook.ExcelWorkBook;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 基础解析器
 *
 * @author xukai
 * @date 2021/12/08 15:46
 **/
public class BaseTemplateParse implements TemplateExcelParse{

    @Override
    public ExcelWorkBook toParse(ExcelContext excelContext) {
        Workbook workBook = excelContext.getTemplateWorkBook();
        ExcelWorkBook excelWorkBook = new ExcelWorkBook();
        if (workBook == null){
            excelContext.intTemplateWorkBook();
            workBook =  excelContext.getTemplateWorkBook();
        }
        Iterator<Sheet> sheetIterator = workBook.sheetIterator();
        while (sheetIterator.hasNext()){
            Sheet sheet = sheetIterator.next();
            Iterator<Row> rowIterator = sheet.rowIterator();
            ExcelSheet excelSheet = new ExcelSheet();
            excelSheet.setSheetName(sheet.getSheetName());
            excelSheet.setSheet(sheet);
            excelSheet.setMergedRegions(sheet.getMergedRegions());

            while (rowIterator.hasNext()){
                Row row = rowIterator.next();
                ExcelRowInterface excelRowInterface = this.toParseRow(row, excelContext, excelSheet);
                if (excelRowInterface != null){
                    excelSheet.getExcelRows().add(excelRowInterface);
                }
            }
            //设置监听器
            if (excelContext instanceof ExcelListenerAble){
                excelSheet.setExcelListenerAble(excelContext);
            }
            //一处合并单元格
            List<Integer> regionIndex = new ArrayList<>();
            for(int i=0; i < sheet.getNumMergedRegions(); ++i) {
                regionIndex.add(i);

            }
            if (CollectionUtils.isNotEmpty(regionIndex)){
                sheet.removeMergedRegions(regionIndex);
            }
            excelWorkBook.getExcelSheets().add(excelSheet);
        }
        return excelWorkBook;
    }

    /**
     * 单独行解析
     * @author xukai
     * @date 2021/12/13 14:23
     * @param row
     * @return com.authine.cloudpivot.excel.row.ExcelRowInterface
     */
    private ExcelRowInterface toParseRow(Row row, ExcelContext excelContext, ExcelSheet excelSheet){
        //是否是子表
        boolean isSub = false;
        Iterator<Cell> cellIterator = row.cellIterator();
        List<ExcelCell> excelCells = new ArrayList<>();
        String group = null;
        while (cellIterator.hasNext()){
            Cell cell = cellIterator.next();
            ExcelCell excelCell = new ExcelCell();
            if (excelContext instanceof ExcelListenerAble){
                excelCell.setExcelListenerAble(excelContext);
            }
            CellStyle cellStyle = cell.getCellStyle();
            CellType cellType = cell.getCellType();
            excelCell.setCellType(cellType);
            excelCell.setCellStyle(cellStyle);
            cell.setCellType(CellType.STRING);

            //获取模板单元格数据
            String stringCellValue = cell.getStringCellValue();

            if (!StringUtils.isEmpty(stringCellValue)){
                if (stringCellValue.indexOf("{") >= 0 || stringCellValue.indexOf("[") >= 0){
                    excelCell.setTitle(false);
                }else {
                    excelCell.setTitle(true);
                }
                if (stringCellValue.indexOf(".") >= 0) {
                    isSub = true;
                }
                if (stringCellValue.indexOf("[") >= 0){
                    excelCell.setIdx(true);
                }
            }
            excelCell.setColumnNum(cell.getColumnIndex());

            excelCell.setTagTem(stringCellValue);
            excelCell.setTagField(this.getField(stringCellValue));
            if (StringUtils.isEmpty(group)){
                group = this.getFieldGroup(stringCellValue);
            }
            excelCells.add(excelCell);
        }
        ExcelRowInterface excelRow;
        //初始化子表行
        if (isSub){
            excelRow = new ExcelRowGroup();
            ExcelRowGroup excelRowGroup = (ExcelRowGroup)excelRow;
            excelRowGroup.setSubName(group);
        }else{//普通行
            excelRow = new ExcelRow();
        }
        excelRow.setIndexNum(row.getRowNum());
        excelRow.setCells(excelCells);
        excelRow.setRow(row);
        excelRow.setExcelSheet(excelSheet);
        return excelRow;
    }

    //获取组名
    private String getFieldGroup(String tempField){
        if (StringUtils.isNotEmpty(tempField)){
            if (StringUtils.contains(tempField, ".")){
                String replace = tempField.replace("{", "").replace("}", "");
                int i = replace.indexOf(".");
                if ( i< 0){
                    return replace;
                }
                return replace.substring(0, replace.indexOf("."));
            }else {
               return null;
            }
        }else {
            return null;
        }

    }

    //获取字段名
    private String getField(String tempField){
        if (StringUtils.isNotEmpty(tempField)){
            if (StringUtils.contains(tempField, "[")){
                return tempField.replace("[", "").replace("]", "");
            }else {
                String replace = tempField.replace("{", "").replace("}", "").replace("%", "");
                int i = replace.indexOf(".");
                if (i < 0){
                    return replace;
                }
                return replace.substring(replace.indexOf(".") + 1);
            }
        }else {
            return "";
        }

    }
}

