package com.authine.cloudpivot.excel.parse.constant;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 表达式常量
 *
 * @author xukai
 * @date 2021/12/22 10:30
 **/
public class ExpressionConstant {

    //表达式
    public static final String FIELD_MATCHING = "\\{{1}[a-zA-Z0-9\\.]+\\}|\\[[0-9]+\\]|\\{\"[a-zA-Z0-9\\.]+\"\\:\\{(\"\\w*\"\\:\"\\w*\",{0,1})+\\}\\}";
    //子表
    public static final String FIELD_MATCHING_SUB ="[a-zA-Z]+[.]+[a-zA-Z]+";
    //序列号
    public static final String FIELD_MATCHING_INDEX = "\\[[0-9]+\\]";
    //选择框
    public static final String FIELD_MATCHING_SELECT ="\\{\"\\w*\\.{0,1}\\w*\"\\:\\{(\"\\w*\"\\:\"\\w*\",{0,1})+\\}\\}";
/*
    //基础的匹配{afs}
    public static final String FIELD_MATCHING_BASIC = "\\{[a-z0-9A-z]+\\}";
    //子基础匹配{a.b}
    public static final String FIELD_MATCHING_SUB_BASIC = "\\{[a-z0-9A-z]+\\.[a-z0-9A-z]+\\}";
    //选择{"afs":{"ad":"adv", "ac":"adv"}}
    public static final String FIELD_MATCHING_BASIC_SELECT = "\\{\"\\w+\"\\:\\{(\"\\w*\"\\:\"\\w*\",{0,1})+\\}\\}";
    //选择{"a.b":{"ad":"adv", "ac":"adv"}}
    public static final String FIELD_MATCHING_SUB_BASIC_SELECT = "\\{\"\\w+\\.{1}\\w+\"\\:\\{(\"\\w*\"\\:\"\\w*\",{0,1})+\\}\\}";*/

    public static List<String> expressionValue(List<String> matcherStr){
        if (CollectionUtils.isEmpty(matcherStr)){
            return null;
        }
        List<String> objects = Lists.newArrayList();
        for (String str : matcherStr){
            String s = StringUtils.removeEnd(StringUtils.removeStart(str, "{"), "}");
            objects.add(s);
        }
        return objects;
    }
/*
    public static List<String> expressionFiled(List<String> matcherStr){
        if (CollectionUtils.isEmpty(matcherStr)){
            return null;
        }
        List<String> objects = Lists.newArrayList();
        for (String str : matcherStr){
            if (isMatcherBasic(str)){
                String s = StringUtils.removeEnd(StringUtils.removeStart(str, "{"), "}");
                objects.add(s);
            }
            if (isMatcherSubBasic(str)){
                String s = StringUtils.removeEnd(StringUtils.removeStart(str, "{"), "}");
                String substring = StringUtils.substring(s, StringUtils.indexOfAny(s, "."));
                objects.add(substring);
            }
            if (isMatcherBasicSelect(str)){
                JSONObject jsonObject = JSONObject.parseObject(str);
                Set<String> keys = jsonObject.keySet();
                objects.addAll(keys);

            }
            if (isMatcherSubBasicSelect(str)){
                JSONObject jsonObject = JSONObject.parseObject(str);
                Set<String> keys = jsonObject.keySet();
                objects.addAll(keys);

            }
        }
        return objects;
    }

    public static List<String> expressionSub(List<String> matcherStr){
        if (CollectionUtils.isEmpty(matcherStr)){
            return null;
        }
        List<String> objects = Lists.newArrayList();
        for (String str : matcherStr){
            if (isMatcherSubBasic(str)){
                String s = StringUtils.removeEnd(StringUtils.removeStart(str, "{"), "}");
                String substring = StringUtils.substring(s,0, StringUtils.indexOfAny(s, "."));
                objects.add(substring);
            }
            if (isMatcherSubBasicSelect(str)){
                JSONObject jsonObject = JSONObject.parseObject(str);
                Set<String> keys = jsonObject.keySet();
                objects.addAll(keys);

            }
        }
        return objects;
    }*/

    public static boolean isMatcher(String str){
        Pattern r = Pattern.compile(FIELD_MATCHING);
        Matcher m = r.matcher(str);
        return m.find();
    }

    public static List<String> matcherResult(String str){
        Pattern r = Pattern.compile(FIELD_MATCHING);
        Matcher m = r.matcher(str);
        List<String> objects = Lists.newArrayList();
        while (m.find()){
            objects.add(m.group());
        }
        return objects;
    }

    public static boolean isMatcherSub(String str){
        Pattern r = Pattern.compile(FIELD_MATCHING_SUB);
        Matcher m = r.matcher(str);
        return m.find();
    }

    public static List<String> matcherSubResult(String str){
        Pattern r = Pattern.compile(FIELD_MATCHING_SUB);
        Matcher m = r.matcher(str);
        List<String> objects = Lists.newArrayList();
        while (m.find()){
            objects.add(m.group());
        }
        return objects;
    }

    public static boolean isMatcherIndex(String str){
        Pattern r = Pattern.compile(FIELD_MATCHING_INDEX);
        Matcher m = r.matcher(str);
        return m.find();
    }

    public static List<String> matcherIndexResult(String str){
        Pattern r = Pattern.compile(FIELD_MATCHING_INDEX);
        Matcher m = r.matcher(str);
        List<String> objects = Lists.newArrayList();
        while (m.find()){
            objects.add(m.group());
        }
        return objects;
    }

    public static boolean isMatcherSelect(String str){
        Pattern r = Pattern.compile(FIELD_MATCHING_SELECT);
        Matcher m = r.matcher(str);
        return m.find();
    }

    public static List<String> matcherSelectResult(String str){
        Pattern r = Pattern.compile(FIELD_MATCHING_SELECT);
        Matcher m = r.matcher(str);
        List<String> objects = Lists.newArrayList();
        while (m.find()){
            objects.add(m.group());
        }
        return objects;
    }

/*
    public static boolean isMatcherBasic(String str){
        Pattern r = Pattern.compile(FIELD_MATCHING_BASIC);
        Matcher m = r.matcher(str);
        return m.matches();
    }

    public static boolean isMatcherSubBasic(String str){
        Pattern r = Pattern.compile(FIELD_MATCHING_SUB_BASIC);
        Matcher m = r.matcher(str);
        return m.matches();
    }

    public static boolean isMatcherBasicSelect(String str){
        Pattern r = Pattern.compile(FIELD_MATCHING_BASIC_SELECT);
        Matcher m = r.matcher(str);
        return m.matches();
    }

    public static boolean isMatcherSubBasicSelect(String str){
        Pattern r = Pattern.compile(FIELD_MATCHING_SUB_BASIC_SELECT);
        Matcher m = r.matcher(str);
        return m.matches();
    }



    public static List<String> matcherBasic(String str){
        Pattern r = Pattern.compile(FIELD_MATCHING_BASIC);
        Matcher m = r.matcher(str);
        List<String> objects = Lists.newArrayList();
        while (m.find()){
            objects.add(m.group());
        }
        return objects;
    }

    public static List<String> matcherSubBasic(String str){
        Pattern r = Pattern.compile(FIELD_MATCHING_SUB_BASIC);
        Matcher m = r.matcher(str);
        List<String> objects = Lists.newArrayList();
        while (m.find()){
            objects.add(m.group());
        }
        return objects;
    }

    public static List<String> matcherBasicSelect(String str){
        Pattern r = Pattern.compile(FIELD_MATCHING_BASIC_SELECT);
        Matcher m = r.matcher(str);
        List<String> objects = Lists.newArrayList();
        while (m.find()){
            objects.add(m.group());
        }
        return objects;
    }

    public static List<String> matcherSubBasicSelect(String str){
        Pattern r = Pattern.compile(FIELD_MATCHING_SUB_BASIC_SELECT);
        Matcher m = r.matcher(str);
        List<String> objects = Lists.newArrayList();
        while (m.find()){
            objects.add(m.group());
        }
        return objects;
    }*/
}

