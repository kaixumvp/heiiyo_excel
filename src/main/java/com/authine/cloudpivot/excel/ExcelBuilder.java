package com.authine.cloudpivot.excel;

import com.authine.cloudpivot.excel.context.BaseExcelContext;
import com.authine.cloudpivot.excel.context.ExcelContext;

import java.io.InputStream;

/**
 * Excel环境构建者
 *
 * @author xukai
 * @date 2021/12/13 14:54
 **/
public class ExcelBuilder {

    /**
     * 构建excel上下文
     * @author xukai
     * @date 2021/12/14 10:12
     * @param inputStream 输入流
     * @return com.authine.cloudpivot.excel.context.ExcelContext
     */
    public static ExcelContext buildExcelContext(InputStream inputStream){
        return BaseExcelContext.build(inputStream);
    }

}

